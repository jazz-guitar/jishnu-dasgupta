# JISHNU DASGUPTA #

JISHNU DASGUPTA
JISHNUDGOFFICIAL
JAZZ GUITARIST
BANGALORE INDIA
FREE JAZZ GUITAR LESSONS
JAZZ WORKSHOP ONLINE
One of the most promising young guitar players in jazz in India, Jishnu Dasgupta brings a contemporary, adventurous sound to compositions jazz musicians have been playing for more than 60 years.

A jazz guitarist based in Bangalore, India, Jishnu Dasgupta is part of a younger breed of jazz musicians in India, making music that is forward-looking whilst being grounded in the jazz guitar tradition of classic jazz standards.

His accessible yet intense style of jazz guitar improvisation, fluid lyricism and ability to expand the given harmonic structure of how to play a jazz standard results in a jazz guitar style that takes hard-bop chromatic phrasing and turns it on its head.

He has played within the many different jazz guitar styles such as bebop jazz, jazz rock fusion guitar and smooth jazz guitar, and is well-known for his use of an 8 string guitar for jazz and in-depth exploration of advanced jazz chord harmony and jazz music theory in jazz standards.

Jishnu got his start as a young jazz guitarist in Bangalore, cutting his teeth playing bebop jazz with Indian music scene veteran Tony Das. Since then he has performed with a number of jazz musicians in India.

An experienced instructor of jazz guitar lessons with a dedicated following, Jishnu is also known for his vocal opposition to the popular belief that “learning jazz guitar seems tough on a good day, and impossible on a bad day”.

To dispel this persistent misconception, he has consistently worked at making learning jazz pedagogy more easily accessible, fun, easy to get started and more enjoyable through his free online jazz guitar lessons, master-classes and online workshops on how to start learning jazz guitar.

A self-confessed autodidact, Jishnu’s jazz guitar youtube lessons and articles on jazz guitar theory hint at an advanced understanding of music theory. His online guitar lessons often make references to the research of Dmitri Tymoczko, the musical analyses of Heinrich Schenker, and tonal frameworks of Neo-Riemannian music theory concepts.

As a jazz guitar teacher, one of Jishnu’s significant recent contributions to the online community of jazz guitarists has been his adaptation of classical Hanon etudes into an improvisational language and format suitable for jazz guitar improvisation.

http://jazzworkshoponline.com

https://jazz-workshop-online.business.site

https://goo.gl/maps/EhgmWeo6NqG7jppB6

https://www.facebook.com/jishnudgofficial

https://twitter.com/jishnudg_jazz

https://www.instagram.com/jishnudgofficial/

http://www.blogtalkradio.com/jazz-guitar
https://disqus.com/by/jazz_guitar
https://www.intensedebate.com/people/jazz_guitar
https://issuu.com/jazz-guitar
http://jazz-guitar.jigsy.com
https://jazz-guitar1.livejournal.com/profile
https://medium.com/@jazz_guitar
https://www.mioola.com/jazzguitar
https://myblogu.com/profile/jazz_guitar
http://jazz-guitar.pen.io
https://jazzguitar.pb.online
http://jazz-guitar.soup.io
https://jazz-guitar1.tumblr.com
https://jazz-guitar.weebly.com
https://jazzguitar.food.blog/
https://jazz-guitar.writeas.com
https://jazz-guitar1.blogspot.com/

http://43marks.com/jazz-guitar
https://www.diigo.com/profile/jazz-guitar
https://jazz-guitar.dropmark.com
http://www.folkd.com/user/jazz-guitar
https://www.instapaper.com/p/jazzguitar
https://www.librarything.com/profile/jazz-guitar
http://www.myhq.com/public/j/a/jazz-guitar
https://in.pinterest.com/jazz_guitar1
https://in.pinterest.com/jazz_guitar_official/
https://wanelo.co/jazz_guitar
https://weheartit.com/jazz_guitar
https://www.wishlistr.com/jazz_guitar

https://www.adsoftheworld.com/user/jazzguitar
https://angel.co/jazzguitar
https://www.apsense.com/user/jazzguitar
https://www.biggerpockets.com/users/jazzguitar
https://www.coroflot.com/jazzguitar
https://www.ebay.com/usr/jazz-guitar1
https://www.edocr.com/user/jazz-guitar
https://www.etsy.com/people/joapseav
https://boards.fool.com/profile/jazzguitar1/info.aspx
https://www.kiva.org/lender/jazzguitar
https://www.linkedin.com/in/jazz-guitar
http://www.profnetconnect.com/jazz-guitar
https://www.ryze.com/go/jazzguitar1
https://siftery.com/company/jazz-guitar
https://jazz-guitar1.slack.com
https://www.slideserve.com/jazz_guitar
https://www.tradingview.com/u/jazz-guitar
https://www.storeboard.com/jazzguitar
https://www.trepup.com/jazzguitar
*
https://independent.academia.edu/JAZZGUITAR
https://www.bebee.com/bee/jazz-guitar
https://www.bebee.com/@jazz-guitar
https://www.blurb.com/user/jazz-guitar
http://bookmooch.com/jazzguitar
https://community.catster.com/profile/jazz_guitar
https://www.crowdrise.com/jazz-guitar
https://community.dogster.com/profile/jazz_guitar
https://www.empire.kred/JAZZ_GUITAR
https://eventful.com/users/jazz_guitar/events
https://www.faceparty.com/jazz-guitar
https://www.fanfiction.net/~jazzguitar
http://www.fixya.com/users/jazz-guitar
https://www.fluther.com/users/jazz_guitar
http://follr.me/jazz-guitar
https://www.goodreads.com/jazz_guitar
https://hubpages.com/@jazz-guitar
https://www.kickstarter.com/profile/jazz-guitar
https://www.linkcentre.com/profile/jazzguitar
https://www.minds.com/jazzguitar
https://myspace.com/jazz-guitar
https://www.pinkbike.com/u/jazz-guitar
https://www.postcrossing.com/user/jazz-guitar
http://jazz-guitar.freeforums.net
https://www.slideshare.net/JAZZGUITAR2
http://jazz-guitar.spruz.com
https://sqeeqee.com/jazz-guitar
http://www.tagged.com/jazz-guitar
https://profiles.tigweb.org/jazz-guitar
https://in.toluna.com/people/jazz-guitar
https://tribyo.com/users/jazzguitar
https://untappd.com/user/jazz-guitar
https://veggsocial.com/jazz-guitar
https://voat.co/u/jazz-guitar
http://writing.com/authors/jazz-guitar
http://www.yuuby.com/jazz_guitar

https://www.artfire.com/ext/people/jazz-guitar
https://www.behance.net/jazz-guitar/
https://www.burdastyle.com/profiles/jazzguitar
https://www.canva.com/jazz-guitar
https://www.chairish.com/shop/jazz-guitar
https://www.colourlovers.com/lover/jazz-guitar
https://coolors.co/u/jazz_guitar
http://www.chictopia.com/jazz_guitar
https://www.designspiration.net/jazz-guitar
https://gigbucks.com/user/jazzguitar
https://www.houzz.in/user/jazzguitar
http://hoverboard.io/jazz_guitar
https://jazz-guitar.jimdosite.com
http://lookbook.nu/jazz_guitar
https://www.pledgemusic.com/people/jazz-guitar
https://society6.com/jazz-guitar
https://www.vecteezy.com/members/jazz-guitar

https://9gag.com/u/jazz_guitar
https://www.chess.com/member/jazz-guitar
https://www.cracked.com/members/jazz-guitar/
https://www.destructoid.com/?name=jazz-guitar
https://www.ebaumsworld.com/user/profile/jazz_guitar
http://www.fanpop.com/fans/jazz-guitar
https://jazz-guitar.guildlaunch.com
https://profile.cheezburger.com/jazz-guitar
https://jazz-guitar.cheezburger.com
https://www.icheckmovies.com/profiles/jazz-guitar
https://i.thechive.com/jazz-guitar
https://people.ign.com/jazz-guitar
https://www.jigidi.com/user/jazz_guitar
https://knowyourmeme.com/users/jazz-guitar
https://www.kongregate.com/accounts/jazz_guitar
https://letterboxd.com/jazz_guitar
https://jazzguitar.listal.com
https://secure.metacritic.com/user/jazz-guitar
https://www.moddb.com/members/jazz-guitar
https://n4g.com/user/score/jazz-guitar
https://www.patreon.com/user/creators?u=21989491
https://player.me/jazz-guitar
https://www.ranker.com/profile-of/jazzguitar
https://www.redbubble.com/people/jazz-guitar
https://www.sbnation.com/users/jazz-guitar
http://www.shockwave.com/member/profiles/jazzguitar.jsp
http://www.singsnap.com/karaoke/member/jazz-guitar
https://www.stage32.com/profile/712428/about
https://trakt.tv/users/jazz-guitar
https://www.wattpad.com/user/jazz-guitar

https://www.allrecipes.com/cook/jazz-guitar
https://jazz-guitar.fandom.com
https://www.fatsecret.com/member/JAZZ+GUITAR
https://www.fitday.com/fitness/forums/members/jazz-guitar.html 
https://keeprecipes.com/jazz-guitar
https://www.menuism.com/users/jazz_guitar
https://www.myfitnesspal.com/profile/jazz_guitar
https://www.sparkpeople.com/mypage.asp?id=JAZZ-GUITAR
https://steepster.com/jazz-guitar
https://steepster.com/places/6182-jishnu-dasgupta-bangalore-karnataka
https://www.strava.com/athletes/jazz-guitar

https://about.me/jazz-guitar
https://www.allmyfaves.com/jazzguitar
https://ask.fm/jazz_guitar
https://www.bloglovin.com/@jazzguitar
https://calendly.com/jazz-guitar
http://dojo.press/user/jazz-guitar
https://new.edmodo.com/profile/jazz-guitar
https://flightaware.com/user/jazzguitar1
https://flipboard.com/@JAZZ_GUITAR
https://en.gravatar.com/jazzguitar2
https://huffduffer.com/jazzguitar
https://ifttt.com/p/jazzguitar
https://itsmyurls.com/jazz_guitar
https://justpaste.it/jazzguitar
https://www.khanacademy.org/profile/jazzguitar
https://leanpub.com/u/jazz-guitar
https://www.mendeley.com/profiles/jazz-guitar
https://www.mouthshut.com/jazzguitar
https://www.netvibes.com/jazzguitar
https://www.openstreetmap.org/user/jazz-guitar
https://padlet.com/jazzguitar
https://www.pearltrees.com/jazzguitar
http://jazzguitar.pressfolios.com
https://www.rrrather.com/user/profile/jazz_guitar
https://www.studypool.com/jazzguitar
http://www.tacked.com/user/jazz-guitar
https://www.tes.com/teaching-resources/shop/jazz-guitar
https://trello.com/jazz_guitar
https://jazz-guitar.webs.com/

https://ello.co/jazz-guitar
https://foursquare.com/user/550705177
https://foursquare.com/user/550705177/list/jishnu-dasgupta
https://jazzguitar.kentodi.com
https://mstdn.io/@jazz_guitar
https://www.plurk.com/jazz_guitar
http://qooh.me/jazz_guitar
https://ketchup.today/c/twitter-jishnudg_jazz/

https://8tracks.com/jazz-guitar
https://jazz-guitar.bandcamp.com
https://www.bandmix.com/jazz_guitar/
https://blip.fm/jazz_guitar
https://www.concertwindow.com/217772-jazz-guitar
https://www.datpiff.com/profile/jazz_guitar
https://www.discogs.com/user/jazz_guitar
https://freesound.org/people/jazz-guitar
https://genius.com/jazzguitar
https://www.hulkshare.com/jazz-guitar
https://hypem.com/jazz_guitar
https://www.jambase.com/profile/jazz_guitar
https://www.jambase.com/band/jazz-guitar
https://www.last.fm/user/jazz-guitar1
https://musicbrainz.org/user/jazz-guitar
https://musicbrainz.org/artist/6c172213-680f-40fd-89d3-907719b2498f
https://musicbrainz.org/release/151d498f-1e20-454c-ae6f-67efbaf6dbfd
https://musicbrainz.org/label/42c8dff3-9efa-4381-95b5-17c5ce6af6bd
https://www.musixmatch.com/profile/jazz-guitar
https://www.reverbnation.com/artist/jazz_guitar
https://www.songkick.com/users/jazz-guitar
https://www.soundclick.com/members4/default.cfm?memberID=6978351
https://soundcloud.com/jazz-guitar1
https://www.spreaker.com/user/jazz-guitar
https://www.spreaker.com/show/jishnu-dasgupta-jazz-guitarist-bangalore
https://www.ultimate-guitar.com/u/jazzguitar4

https://anchor.fm/jazz-guitar
https://www.dailykos.com/user/jazzguitar
https://www.fark.com/users/jazz-guitar
http://jazzguitar1.podbean.com
https://the-artifice.com/author/jazzguitar
http://www.wikidot.com/user:info/jazz-guitar
http://jazz-guitar.wikidot.com

https://gallery.1x.com/member/jazzguitar
https://500px.com/jazzguitar
https://www.blipfoto.com/jazzguitar
https://www.brusheezy.com/members/jazz-guitar
https://dashburst.com/jazz-guitar
https://www.dreamstime.com/Jazzguitar_info
https://public.fotki.com/jazz-guitar
https://www.freepik.com/collection/jazz-guitar/182288
https://gfycat.com/@jazz-guitar
https://giphy.com/channel/jazz-guitar
https://imageshack.com/user/jazz-guitar
https://imgur.com/user/jazzguitar
https://lightstalking.us/members/jazz-guitar
https://linktr.ee/jazz_guitar
https://www.lomography.com/homes/jazz-guitar
https://www.mobypicture.com/user/jazz_guitar
https://beta.photobucket.com/u/jazz_guitar1
https://photopeach.com/user/jazz-guitar
http://jazz-guitar.piccsy.com
https://picsart.com/jazzguitar8
https://jazz-guitar.picturepush.com
https://pixabay.com/users/jazz-guitar-13035066
https://postimg.cc/user/jazz-guitar
https://postimg.cc/gallery/38ts1cbe6
https://www.purephoto.com/jazz-guitar
http://www.photoshow.com/members/jazz-guitar
https://fancy.com/jazz_guitar
http://www.twitxr.com/jazz_guitar
https://unsplash.com/@jazz_guitar/collections
https://youpic.com/photographer/JAZZGUITAR

https://alternativeto.net/user/jazzguitar
https://bitbucket.org/jazz-guitar
https://bitbucket.org/jazz-guitar/jishnu-dasgupta
https://bitbucket.org/snippets/jazz-guitar
https://www.cnet.com/profiles/jazz-guitar
https://www.codecademy.com/profiles/jazz-guitar
https://www.codementor.io/jazzguitar
https://curious.com/jazz_guitar
https://www.cybrhome.com/jazz_guitar
https://degreed.com/jazzguitar
https://www.f6s.com/jazzguitar
https://www.freecodecamp.org/jazz-guitar
https://github.com/jazz-guitar
https://github.com/jazz-guitar/jishnu-dasgupta/blob/master/jishnudgofficial.md
https://gist.github.com/jazz-guitar/bacd5ac0715b3e58d4466762b7c05183
https://gitlab.com/jazz-guitar
https://gitlab.com/jazz-guitar/jishnu-dasgupta
https://hackaday.io/jazzguitar
https://www.hackerrank.com/jazzguitar
https://www.hackster.io/jazz-guitar
https://www.hackster.io/jazz-guitar/jishnu-dasgupta-7b3fe3
http://www.ipernity.com/home/jazz-guitar
https://www.kaggle.com/jazzguitar
https://www.kaggle.com/jazzguitar/jishnu-dasgupta
https://keybase.io/jazzguitar
https://pastebin.com/u/jazz-guitar
https://www.producthunt.com/@jazz_guitar
https://publiclab.org/profile/jazzguitar
https://republic.co/jazz-guitar
https://scratch.mit.edu/users/jazz-guitar/
https://sketchfab.com/jazz-guitar
https://www.skillshare.com/user/jazz-guitar
https://slashdot.org/~jazzguitar1
https://snipplr.com/users/jazzguitar
https://sourceforge.net/u/jazz-guitar/profile/
https://www.thingiverse.com/jazz_guitar/about

https://www.bewelcome.org/members/jazz-guitar
https://www.gapyear.com/members/jazz-guitar/
https://www.lonelyplanet.com/profile/jazz_guitar
https://www.lovento.com/en/people/jazz_guitar/
https://maps.roadtrippers.com/people/jazz-guitar
https://maps.roadtrippers.com/trips/26351031
https://www.touristlink.com/user/jazz-guitar
https://www.travelblog.org/Bloggers/jazzguitar/
https://www.travelblog.org/Asia/India/Karnataka/Bangalore/blog-1038258.html
https://www.travellerspoint.com/users/jazz-guitar/
https://www.trip.skyscanner.com/user/jazz-guitar
https://www.trip.skyscanner.com/jishnu-dasgupta-jazz-guitarist-bangalore-india-jazz-guitar/map
https://www.trip.skyscanner.com/jazz-guitar-jishnu-dasgupta-trip
https://www.trip.skyscanner.com/bengaluru-bangalore-india/things-to-do/koramangala-bengaluru-karnataka
https://jazz-guitar.trvl.com/

https://creator.wonderhowto.com/jazzguitar/
https://amara.org/en/profiles/profile/jazz-guitar/
https://coub.com/jazz-guitar
https://www.dailymotion.com/dm_ba14b6204a470d396c9ccb9677bf26b2
https://www.flickbin.tv/stations/jazz-guitar
https://www.funnyordie.com/users/jazz_guitar
http://www.metacafe.com/channels/jazz-guitar-jazz/
https://www.smule.com/jazzguitar1
https://www.twitch.tv/jazzguitar1
https://www.veoh.com/users/jazz-guitar
https://www.veoh.com/list/c/jazz-guitar
https://videosift.com/member/jazz-guitar
http://vidhole.com/uploader/jazzguitar/


